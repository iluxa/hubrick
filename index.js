import rules from './rules';
import Processor from './processor';
import colors from 'colors';

const body = `
{
	"color":"red",
	"background-color":"blue",
	"display":"block"
}
`;

const print = (result,last)=>{
	console.log(result.message[result.isSuccess ? 'green' : 'red'], last ? 'End.' : '');
};

let processor = new Processor(body, rules, true);

processor.start((results)=> {
	results.forEach((result,i)=>print(result, (results.length-1)===i));
});