
const EventEmitter = require('events');
class Processor {
    constructor(body, rules = [], debug = false) {
        this.body = this.tryParseBody(body);
        this.firstRule = rules[0];
        this.indexedRules = this.index(rules);
        this.register = {};
        this.results = [];
        this.debug = debug;
        
        this.emmiter = new EventEmitter();
        // Using dynamic event name for each instance to avoid event collisions when runnning multiple processor instances
        this.eventName = (new Date()).getTime();

        // Using pub/sub instead of recursion. Prevent nested callbacks in large data sets
        this.emmiter.on(this.eventName,(rule_id)=>{
            this.nextRule(rule_id);
        });
    }

    // Convert array of rules to key value pairs for faster lookup
    index(rules) {
        return rules.reduce((prev, cur)=> {
            //tryEvalFunc - Converts JS string process function into js function for later execution
            cur.process = this.tryEvalFunc(cur.process);
            prev[cur.id] = cur;
            return prev;
        }, {});
    }

    tryParseBody(body) {
        if (typeof body !== 'string') {
            return body;
        }
        try {
            return JSON.parse(body);
        } catch (e) {
            throw Error('Body is not in correct format');
        }
    }

    tryEvalFunc(funcStr) {
        if (typeof funcStr !== 'string') {
            return funcStr;
        }

        try {
            return eval(funcStr);
        } catch (e) {
            throw Error('Invalid process function string ');
        }
    }
    //Innitial call to start processing
    start(callback) {
        this.onDone = callback;
        this.nextRule(this.firstRule.id);
    }

    addResult(rule, isSuccess) {
        let status = isSuccess ? 'Passed' : 'Failed';
        this.results.push({message:`${rule.title} ${status}`, isSuccess});
    }

    nextRule(rule_id) {
        if (!rule_id) {
            this.done(this.results);
            return null;
        }
        //Prevents circular calls. Returns if rule_id already been processed/registered 
        if (this.register[rule_id] === true) {
            this.log('Error: Circular call detected! Trying to execute rule with id:', rule_id);
            this.done(this.results);
            return null;
        }

        let rule = this.indexedRules[rule_id];

        if (rule) {
            this.register[rule_id] = true;
            //Copy object body to prevent mutations
            let body = Object.assign({}, this.body);
            let isSuccess = rule.process(body);
            this.addResult(rule, isSuccess);
            let nextId = isSuccess ? rule.true_id : rule.false_id;
            // Using pub/sub instead of recursion. Prevent nested callbacks in large data sets
            this.emmiter.emit(this.eventName, nextId);
            return nextId;
        } else {
            this.done(this.results);
            return null;
        }
    }

    done() {
        return this.onDone && this.onDone(this.results);
    }

    log(...args) {
        this.debug && console.log(...args);
    }
}

export default Processor;