var expect = require('expect.js');
import Processor from '../processor';

const rules = [
    {
        title: "Rule 1",
        id: "1",
        true_id: "2",
        false_id: null,
        //Support process function as string, in case of user manual submission
        process: `(function (obj) {return obj['color'] && obj['color']==='red';})`
    },
    {
        title: "Rule 2",
        id: "2",
        true_id: "4",
        false_id: "3",
        process: function (obj) {
            return !!obj['background-image'];
        }
    }
];

const noop = ()=>{};

describe('Processor', function() {
    var processor = new Processor('{}',rules);
    it('Expect Processor to be a new instance', function() {
        expect(processor).to.be.an(Processor);
    });

    it('Expect indexedRules to be indexed by id', function() {
        var indexedRules = processor.index([rules[0]]);
        expect(indexedRules['1']).to.be.ok();
        expect(indexedRules['1'].id).to.be('1');
        expect(indexedRules['1'].process).to.be.an('function');
    });

    it('Expect tryParseBody to build an object', function() {
        var parsedBody = processor.tryParseBody(`{"test":"ok"}`);
        expect(parsedBody).to.be.ok();
        expect(parsedBody).to.eql({test:'ok'});
    });

    it('Expect tryEvalFunction will create function from string', function() {
        var func = processor.tryEvalFunc("(function(){return 'ok';})");
        expect(func).to.be.an('function');
        expect(func()).to.eql('ok');
    });

     it('Expect nextRule function will exit if rule_id is undefined', function() {
        var _processor = new Processor('{}',rules);
        _processor.emmiter = {emit:noop, on:noop}
        var nextRuleId = _processor.nextRule(null);
        expect(nextRuleId).to.eql(null);
    });

     it('Expect nextRule function return next rule id', function() {
        var _processor = new Processor('{"color":"red"}',rules,true);
        _processor.emmiter = {emit:noop, on:noop}
         var nextRuleId = _processor.nextRule("1");
        expect(nextRuleId).to.eql("2");
    });

     it('Expect nextRule function return next rule id of null', function() {
        var _processor = new Processor('{"color":"blue"}',rules);
        _processor.emmiter = {emit:noop, on:noop}
         var nextRuleId = _processor.nextRule("1");
        expect(nextRuleId).to.eql(null);
    });

      it('Expect nextRule function will detect circular invocation', function() {
        var _processor = new Processor('{"color":"blue"}',rules);
        _processor.emmiter = {emit:noop, on:noop}
        _processor.register["1"] =true;
         var nextRuleId = _processor.nextRule("1");
        expect(nextRuleId).to.eql(null);
    });

       it('Expect addResult function builds a correct string', function() {
        var _processor = new Processor('{"color":"blue"}',rules);
        _processor.addResult(rules[0],false);
        _processor.addResult(rules[1],true);
        expect(_processor.results.length).to.eql(2);
        expect(_processor.results[0]).to.eql(`${rules[0].title} Failed`);
        expect(_processor.results[1]).to.eql(`${rules[1].title} Passed`);
    });
});