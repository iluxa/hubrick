const rules = [
    {
        title: "Rule 1",
        id: "1",
        true_id: "2",
        false_id: null,
        //Support process function as string, in case of user manual submission via UI
        process: `(function (obj) {return obj['color'] && obj['color']==='red';})`
    },
    {
        title: "Rule 2",
        id: "2",
        true_id: "4",
        false_id: "3",
        process: function (obj) {
            return !!obj['background-image'];
        }
    },
    {
        title: "Rule 3",
        id: "3",
        true_id: "4",
        false_id: null,
        process: function (obj) {
            return obj['background-color'] && obj['background-color'] === 'blue';
        }
    },
    {
        title: "Rule 4",
        id: "4",
        true_id: null,
        false_id: "5",
        process: function (obj) {
            return obj['display'] && obj['display'] === 'none';
        }
    },
    {
        title: "Rule 5",
        id: "5",
        true_id: null,
        false_id: null,
        process: function (obj) {
            return obj['display'] && obj['display'] === 'block';
        }
    }
];

export default rules;